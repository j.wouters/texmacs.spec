#!/bin/bash
# define the environment variable COPR_WEBHOOK_URL before running the script

COMMIT_HASH=$(curl -s https://api.github.com/repos/texmacs/texmacs/commits/master | jq -r '.sha')
COMMIT_DATETIME=$(curl -s https://api.github.com/repos/texmacs/texmacs/commits/master | jq -r '.commit.author.date')
COMMIT_UNIXTIME=$(date -d $COMMIT_DATETIME '+%s')

read LATEST_NIGHTLY_BUILD_HASH < latest-nightly-build-commit-hash.txt

NOW_UNIXTIME=$(date '+%s')

let "TIME_DIFF = $NOW_UNIXTIME - $COMMIT_UNIXTIME"

#if (( $TIME_DIFF < 24*60*60 )); then
if [ "$LATEST_NIGHTLY_BUILD_HASH" != "$COMMIT_HASH" ]; then
    curl -S -s -o /dev/null -X POST $COPR_WEBHOOK_URL
    echo $COMMIT_HASH > latest-nightly-build-commit-hash.txt
fi
