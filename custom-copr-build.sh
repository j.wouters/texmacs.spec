#! /bin/sh -x
curl https://gitlab.act.reading.ac.uk/ss902791/texmacs.spec/-/raw/master/TeXmacs.spec -O
VERSION=$(curl -s https://api.github.com/repos/texmacs/texmacs/tags | jq -r '.[0].name')
VERSION="${VERSION:1}"
wget https://github.com/texmacs/texmacs/archive/v$VERSION.tar.gz
sed -i -e 's/^Version:[[:space:]]*[0-9]\+\.[0-9]\+\.[0-9]\+.*/Version:    '$VERSION'/g' TeXmacs.spec
