#!/bin/bash
# define the environment variable COPR_WEBHOOK_URL before running the script

LAST_TAG_COMMIT_HASH=$(curl -s https://api.github.com/repos/texmacs/texmacs/tags | jq -r '.[0].commit.sha')
COMMIT_DATETIME=$(curl -s https://api.github.com/repos/texmacs/texmacs/commits/$LAST_TAG_COMMIT_HASH | jq -r '.commit.author.date')
COMMIT_UNIXTIME=$(date -d $COMMIT_DATETIME '+%s')

read LATEST_STABLE_BUILD_HASH < latest-stable-build-hash.txt

NOW_UNIXTIME=$(date '+%s')

let "TIME_DIFF = $NOW_UNIXTIME - $COMMIT_UNIXTIME"

#if (( $TIME_DIFF < 24*60*60 )); then
if [ "$LATEST_STABLE_BUILD_HASH" != "$LAST_TAG_COMMIT_HASH" ]; then
    curl -S -s -o /dev/null -X POST $COPR_WEBHOOK_URL
    echo $LAST_TAG_COMMIT_HASH > latest-stable-build-commit-hash.txt
fi
